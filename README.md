SuperFast WordPress Theme
=====================

Wordpress theme with npm packages & gulp tasks.  

More information available at <a href="https://mpmdevs.com">https://mpmdevs.com</a>

Getting Started
-------------

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.


Prerequisites
-------------

> You will need to have [Node.js](https://nodejs.org/en/) and [NPM](https://www.npmjs.com/) installed in order to build this
> project.




Installing
-------------

1. Download on clone project on your local machine

2. Copy empty directory in your website in themes directory

3. Go to: ***wp-content/themes/empty***

4. Install all the node module package using cmd:
``` npm install ``` 

5. Run gulp task
``` gulp debug ```


Include new script in project
-------------

**Add npm script**

> Add path to script in ***wp-content/themes/[theme_name]/included/vendor_scripts.json***
>
> Run gulp task 
>
>``` gulp vendorScripts ```

**Add custom script**

> Add path to script in ***wp-content/themes/[theme_name]/included/custom_scripts.json***


Include new css in project
-------------

**Add npm style**
> Add path to style in ***wp-content/themes/[theme_name]/css/main.css***

----------


Gupl Tasks
-------------

> The task for optimizing local images from *****wp-content/themes/[theme_name]/images***** directory:
>
> ``` gulp optimizationImages ```

>  The task for  optimizing uploaded images from ***wp-content/uploads*** 
>
> ``` gulp optimizationUploadsImages```

>  Watch css changes for development: 
>
> ``` gulp watchDebug```

>  Combine scripts from node_modules: 
>
> ``` gulp vendorScripts```

>  Combine local scripts from ***wp-content/themes/[theme_name]/js***: 
>
> ``` gulp customScriptsRelease```

>  Prepere for debug: 
>
> ``` gulp debug```

>  Prepere for release:
>
> ``` gulp release```

