
var gulp   = require('gulp'),
    minify = require('gulp-minify'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    del = require('del'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    imagemin = require('gulp-image'),
    vendor_scripts = require('./included/vendor_scripts.json'),
    custom_scripts = require('./included/custom_scripts.json');


gulp.task('watchDebug', function () {
    gulp.watch('./css/**/*.scss', ['styleDebug']);
    gulp.watch('./included/vendor_scripts.json', ['vendorScripts'])
});

gulp.task('release', ['customScriptsRelease', 'styleRelease', 'vendorScripts'], function () {});
gulp.task('debug', ['styleDebug', 'clearJs', 'vendorScripts'], function () {});

gulp.task('vendorScripts', function () {
    del.sync('./js/app_vendor.min.js');
    return gulp.src(vendor_scripts)
        .pipe(concat('app_vendor.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./js/'));
});

gulp.task('customScriptsRelease', function() {
    del.sync('./js/app.min.js');
    return gulp.src(custom_scripts)
        .pipe(concat('app.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./js/'));
});

gulp.task('styleRelease', ['clearCss'], function () {
    return gulp.src('./css/*.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('./css/'));
});

gulp.task('styleDebug', ['clearCss'], function () {
    return gulp.src('./css/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./css/'));
});


gulp.task('optimizationUploadsImages', function() {
    gulp.src('../../uploads/**/*')
        .pipe(imagemin({
            pngquant: true,
            optipng: true,
            zopflipng: false,
            jpegRecompress: false,
            jpegoptim: true,
            mozjpeg: true,
            gifsicle: true,
            svgo: true,
            concurrent: 10
        }))
        .pipe(gulp.dest('../../uploads'));
});

gulp.task('optimizationImages', function() {
    gulp.src('images/**/*')
        .pipe(imagemin({
            pngquant: true,
            optipng: true,
            zopflipng: false,
            jpegRecompress: false,
            jpegoptim: true,
            mozjpeg: true,
            gifsicle: true,
            svgo: true,
            concurrent: 10
        }))
        .pipe(gulp.dest('images'));
});

gulp.task('clearJs', function () {
    del.sync('./js/*.min.js');
});

gulp.task('clearCss', function () {
    del.sync('./css/*.css');
});