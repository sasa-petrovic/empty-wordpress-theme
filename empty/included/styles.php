<?php
function include_styles() {

    if( !is_admin()){
        wp_register_style('main', get_stylesheet_directory_uri() . '/css/main.css', '', THEME_VERSION, 'all');
        wp_enqueue_style('main');
    }
}

add_action( 'wp_enqueue_scripts', 'include_styles' );