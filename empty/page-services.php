<?php
/*
Template Name: Service tpl
*/
?>
<?php get_header(); ?>

<div id="page_w">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
		<article class="post" id="post-<?php the_ID(); ?>">
            
            <div class="entry">


                <!--------SERVICES----------->
                <div id="services-01_w" class="page-01_top">
                    <div id="page-top-box-all">
                        <div id="services-01">
                            <h1>First Ride Offer</h1>
                            <h2>GET 30% off your first ride</h2>

                            <h3>Offer is valid for</h3>

                            <div class="services-box-top">
                                <a href="<?php echo home_url() ?>/services/weddings/">
                                    <img src="<?php echo home_url(); ?>/wp-content/uploads/2015/06/wedding-img.png" alt="email-contact" class="alignnone size-full wp-image-45" />
                                    <h6>Wedding Limo Service</h6>
                                </a>
                            </div>

                            <div class="services-box-top">
                                <a href="<?php echo home_url(); ?>/services/airport-transportation/">
                                    <img src="<?php echo home_url(); ?>/wp-content/uploads/2015/06/airport-img.png" alt="email-contact" class="alignnone size-full wp-image-45" />
                                    <h6>Airport Transportation</h6>
                                </a>
                            </div>

                            <div class="services-box-top">
                                <a href="<?php echo home_url(); ?>/services/corporate-service/">
                                    <img src="<?php echo home_url(); ?>/wp-content/uploads/2015/06/corporate-img.png" alt="email-contact" class="alignnone size-full wp-image-45" />
                                    <h6>Corporate Service</h6>
                                </a>
                            </div>

                            <div class="services-box-top">
                                <a href="<?php echo home_url(); ?>/services/prom-service/">
                                    <img src="<?php echo home_url(); ?>/wp-content/uploads/2015/06/prom-img.png" alt="email-contact" class="alignnone size-full wp-image-45" />
                                    <h6>Prom Service</h6>
                                </a>
                            </div>
                            

                            <div class="cl"></div>
                        </div>
                    </div>
                </div>


                <?php echo do_shortcode('[primary_nav]'); ?>

                <?php /*?><div id="services-02_w" class="airport-bg">
                    <div id="services-02">
                        <div class="services-02-lf">
                            <div class="services-02-top">
                                <h2>Airport Transportation</h2>
                                <h5>Arrive refreshed in our super comfortable limos</h5>
                            </div>

                            <div class="services-02-bt no-padding-bt">
                                <p>Whether in a hurry to the airport, visiting a family member out of town or transporting a special client to a meeting, Arrow Point Worldwide Group can accommodate all of your car service needs. Etiam fringilla, orci eu volutpat sagittis, elit nunc ullamcorper ligula.</p>
                                <p>Mauris tincidunt tortor justo, vitae viverra ante sodales at. Ut mauris neque, bibendum vitae convallis vitae, interdum sed tellus. Nunc id massa ut nulla suscipit tristique. Nullam sed nisi id felis elit nunc ullamcor!</p>
                            </div>
                        </div>

                        <div class="services-02-rh">
                            <img src="<?php echo home_url(); ?>/wp-content/uploads/2015/06/airport-serv-img.png" alt="email-contact" class="alignnone size-full wp-image-45" />
                        </div>
                        
                        <div class="services-02-text">
                            <p>Clients or groups wanting to schedule airport transportation to or from either airport can call the company at 505-470-8087 to make a reservation or click on Book Now featured above and complete the company’s reservation on-line reservation form. AWG is on call 24/7 for safe dependable luxury limousine service to the airports in Santa Fe and Albuquerque.</p>
                            <p>Airport shuttle transportation to the Albuquerque airport and the Santa Fe airport is usually provided by vans and large charter style buses that offer pick-ups at the local hotels in both cities and at both airport locations. However AWG offers Albuquerque airport shuttle service and Santa Fe Airport service to both individuals and groups so for people needing shuttle service without the fuss and hassle of being on a bus or in a van, those folks should call AWG and leave the driving to us. For the ultimate in Albuquerque and Santa Fe airport shuttle service, ride in a limo with us and make the trip to either destination or from the airport an amazing pleasurable comfortable experience. Call AWG at 505-470-8087 for Albuquerque airport and Santa Fe airport limo transportation.</p>
                            <p>AWG transportation to the Albuquerque airport and the Santa Fe airport is usually provided by vans and large charter style buses that offer pick-ups at the local hotels in both cities and at both airport locations. However AWG offers Albuquerque airport shuttle service and Santa Fe Airport service to both individuals and groups so for people needing shuttle service without the fuss and hassle of being on a bus or in a van, those folks should call AWG and leave the driving to us.</p>
                            <h6>For the ultimate in Albuquerque and Santa Fe airport shuttle service, ride in a limo with us and make the trip to either destination or from the airport an amazing pleasurable comfortable experience. Call AWGat 505-470-8087 </h6>
                        </div>

                    </div>
                </div><?php */?>

                    <?php the_content(); ?>

               

                <div class="cl"></div>
            </div>


        </article>

    <?php endwhile; endif; ?>

<?php get_footer(); ?>
